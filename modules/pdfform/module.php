<?php

$Module = array( 'name' => 'ng PDF form',
                 'variable_params' => true );

$ViewList = array();
$ViewList['submit'] = array(
    'script' => 'submit.php',
    'functions' => array( 'submit' ),
    'params' => array(),
    'post_actions' => array( ),
    'unordered_params' => array( ) );

$ViewList['download'] = array(
    'script' => 'download.php',
    'functions' => array( 'download' ),
    'params' => array ( 'ObjectID' ),
    'post_actions' => array( ),
    'unordered_params' => array( ) );

$ViewList['popup'] = array(
    'script' => 'popup.php',
    'functions' => array( 'popup' ),
    'params' => array ( 'ObjectID' ),
    'post_actions' => array( ),
    'unordered_params' => array( ) );

$FunctionList['submit'] = array( );
$FunctionList['download'] = array( );
$FunctionList['popup'] = array( );

?>
