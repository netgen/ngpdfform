<?php

$Module = $Params['Module'];

if (!is_numeric($ObjectID))
    eZExecution::cleanExit();

$object = eZContentObject::fetch( $ObjectID );

if (!is_object($object))
    eZExecution::cleanExit();

$pdfFormINI = eZINI::instance('pdfform.ini');

// we need the mater file
if ( $pdfFormINI->hasVariable('Download', 'MasterFileName') )
    $masterPdfNameArray =  $pdfFormINI->variable('Download', 'MasterFileName');//"master.pdf";
else
    return $Module->handleError( eZError::KERNEL_NOT_AVAILABLE, 'kernel' );

$classIdentifier = $object->attribute('class_identifier');
$classIdentifierParts = explode( '_', $classIdentifier );
$type = strtoupper( $classIdentifierParts[1] );

if ( $type != 'AP' && $type != 'LD' )
    return $Module->handleError( eZError::KERNEL_NOT_AVAILABLE, 'kernel' );

$masterPdfName = $masterPdfNameArray[$type];

if ( empty( $masterPdfName ) )
    return $Module->handleError( eZError::KERNEL_NOT_AVAILABLE, 'kernel' );

$master_pdf = eZURLOperator::eZDesign( eZTemplate::factory(), "files/$masterPdfName", "ezdesign" );

// defining intermediate files
$varDir = eZSys::varDirectory();
if (!is_dir( $varDir."/cache/saved_files_pdfform" ))
    mkdir( $varDir."/cache/saved_files_pdfform" );

$fdf_file = $varDir."/cache/saved_files_pdfform/". $ObjectID .".fdf";
$pdf_file = $varDir."/cache/saved_files_pdfform/". $ObjectID.".pdf";

$user = eZUser::currentUser();

// creating FDF file
if ( $pdfFormINI->hasVariable('Download', 'AttributeMap') )
{
    $fields = array();
    $objectDataMap = $object->attribute('data_map');
    foreach( $pdfFormINI->variable('Download', 'AttributeMap') as $key => $multiValue)
    {
        if ( $multiValue == 'ObjectID' )
        {
            $fields[$key] = $ObjectID;
        }
        elseif( $multiValue == 'userID' )
        {
            $fields[$key] = $user->attribute('contentobject_id');
        }
        elseif( $multiValue == 'userObjectName' )
        {
            $fields[$key] = $user->attribute('contentobject')->attribute('name');
        }
        else
        {
            //explode values by comma from AttributeMap in ini file
            $valueArray = explode( ',', $multiValue );

            $multipleFields = array();
            foreach( $valueArray as $value )
            {
                $attributeObject = $objectDataMap[$value];

                if ( $attributeObject->attribute('content') instanceof eZTags )
                {
                    $multipleFields[] = $attributeObject->attribute('content')->attribute('keyword_string');
                }
                elseif ( $attributeObject->attribute('content') instanceof eZBirthday )
                {
                    $tpl = eZTemplate::factory();
                    $tpl->setVariable( "attribute", $attributeObject );
                    $multipleFields[] = $tpl->fetch( "design:content/datatype/view/ezbirthday.tpl" );

                }
                else
                {
                    $multipleFields[] = $attributeObject->attribute('content');
                }
            }
            //assign values to key from ini file
            //$fields[$key] = 'TESTING 1234 čćžđš ČĆŽŠĐ';
            $fields[$key] = implode(', ', $multipleFields );
        }

    }
}

//$error_level = error_reporting();
//error_reporting($error_level ^ E_NOTICE);
$error = 0;
//$rootPath = '/var/www/ez/sense/rgm-2012.6';
$file = $rootPath . ltrim ($master_pdf,'/');
$flatten = false;
$operation = "";
$fill_func = 'fill_pdf_file_1';

if ( $pdfFormINI->hasVariable('Download', 'DownloadPDFFileName') )
{

    $downloadPDFFileName = $pdfFormINI->variable('Download', 'DownloadPDFFileName');
    $download_file_name =  $downloadPDFFileName[$type]. $ObjectID . '.pdf';
    //echo 'dasdasd  '.$download_file_name;die();
}
else
{
    $download_file_name = $ObjectID.".pdf";
}


//set field values to pdf
//if ( $pdfFormINI->variable('Download', 'FDFFormat') == 'xfdf')
//{
    //$fdf_content = createXFDF($master_pdf, $fields);
    pdfff_fill_pdffile_and_dump_to_http($file, $fill_func, $operation, $flatten, $fields, $download_file_name);
//}
//else
//{
    //$fdf_content = createFDF($master_pdf, $fields);
    //pdfff_fill_pdffile_and_dump_to_http($file, $fill_func, $operation, $flatten, $fields);
//}


//error_reporting($error_level);

//file_put_contents($fdf_file, $fdf_content);
eZExecution::cleanExit();
// download (direct or indirect)
/*
if ( $pdfFormINI->variable('Download', 'ShowTemplate') == 'enable') {
    passthru("pdftk $master_pdf fill_form $fdf_file output $pdf_file");

    $tpl = eZTemplate::factory();
    $tpl->setVariable( 'filename', '/'.$pdf_file );

    $Result = array();
    $Result['content'] = $tpl->fetch( 'design:pdfform_download.tpl' );

} else {

    $download_file_name = $ObjectID.".pdf";
    if ( $pdfFormINI->hasVariable('Download', 'DownloadPDFFileName') )
        $download_file_name = $pdfFormINI->variable('Download', 'DownloadPDFFileName');

    header('Content-type: application/pdf');
    header('Content-Disposition: attachment; filename="'.$download_file_name.'"');
    passthru("pdftk $master_pdf fill_form $fdf_file output - ");
    eZExecution::cleanExit();
}*/


// (C) copyleft AGPL license, http://itextpdf.com/terms-of-use/agpl.php, Nikolay Kitsul.

// Note: Use proper escaping functions: pdfff_escape($str), pdfff_checkbox($value).

/*$error_level = error_reporting();
error_reporting($error_level ^ E_NOTICE);
$error = 0;
*/
// Do not pass parameters from $_REQUEST to proc_open() without validation!
/*$allowed_files = array('pdf_file_1', 'pdf_file_2');

if (in_array($_REQUEST['file'], $allowed_files))
    $file = $master_pdf;
else {
    echo 'Unknown file.';
    exit;
}*/
//$file = $master_pdf;

// Process parameters.
//if ($_REQUEST['flatten'])
    //$flatten = true;
//else
    //$flatten = false;

//if ($_REQUEST['pff_op'] == 'list')
//    $operation = "list";
//else if ($_REQUEST['pff_op'] == 'env_vars')
    //$operation = "env_vars";
//else if ($_REQUEST['pff_op'] == 'dump_fields')
//    $operation = "dump_fields";
//else
//    $operation = null;

/*switch ($_REQUEST['file']) {
    case 'pdf_file_1':*/
        //$fill_func = 'fill_pdf_file_1';
        /*break;
    case 'history_2':
        $fill_func = 'fill_pdf_file_2';
        break;
}*/
/*print_r( $file );die();
pdfff_fill_pdffile_and_dump_to_http($file, $fill_func, $operation, $flatten);

error_reporting($error_level);*/

/**
 * Fills a pdf file <var>$file</var> with the function provided <var>$fill_func</var> and dumps it to http for the browser to download.
 *
 * @param string $file pdf filename to fill.
 * @param string $fill_func - function name of a function that will fills pdf file by writing to a pipe.
 * @param string $operation   'fill' - DEFAULT. Fill pdf file <var>$file</var> with what is in function <var>$fill_func</var>. <br>
 *                          'list' - List all fields that are available in pdf file $file. <br>
 *                          'env_vars' - List shell enviroment variables. Useful if returned error code is 127 (when paths are wrong).<br>
 *                          'dump_fields' - Dump fields, submitted to be submitted to phpformfiller, to browser. Useful if Unix Shell encoding is wrong.<br>
 *
 * @param boolean $flatten - produce "flat" pdf, that is its forms will not be editable any more.
 */
function pdfff_fill_pdffile_and_dump_to_http($file, $fill_func, $operation = 'fill', $flatten = false, $fields, $download_file_name) {
    $DO_DUMP = false;

    if ($flatten)
        $flatten = " -flatten";
    else
        $flatten = "";

//putenv('LANG=en_US.UTF-8');
    $env = array('LANG' => 'en_US.UTF-8');
    $path = null;
// On FreeBSD open_proc() is not run in a shell and
// thus a PATH with 'usr/local/bin' and CWD are not set.
// $env['PATH'] = '/usr/bin:/bin:/usr/local/bin';
//$path = '/var/www/ez/sense/rgm-2012.6';

    if ($operation == 'list') {
        // List all fields that are available in pdf file $file.
        $cmd = 'java -jar pdfformfiller.jar ' . $file . ' -l 2>&1';
        $DO_DUMP = true; // send error as text to browser (not as a file to download via http).
    } else if ($operation == 'env_vars') {
        $cmd = 'set';
        $DO_DUMP = true;
    } else if ($operation == 'dump_fields') {
        $cmd = 'cat';
        $DO_DUMP = true;
    } else
        $cmd = 'java -jar pdfformfiller.jar ' . $file . ' -font "'.$path.'extension/ngpdfform/design/standard/fonts/GHEAGrpalatReg.ttf"' . $flatten . ' 2>&1';
        //$cmd = 'java -jar pdfformfiller.jar ' . $file . $flatten . ' 2>&1';

    $descriptorspec = array(
        0 => array("pipe", "r"), // stdin is a pipe that the child will read from
        1 => array("pipe", "w"), // stdout is a pipe that the child will write to
        // 2 => array("file", "error-output.txt", "a") // stderr is a file to write to
        2 => array("pipe", "w")  // Actually, stderr is sent to stdin " 2>&1"
            // in $cmd above, as select() in php is not reliable.
    );

    $f = proc_open($cmd, $descriptorspec, $pipes, $path, $env /* [, array $other_options ] */);
    $error |= $f === false;

    if (!$operation || $operation == 'fill')
        $error |= $fill_func($pipes[0], $fields);

    $error |= false === fclose($pipes[0]);

    $result = stream_get_contents($pipes[1]);
    $error |= false === $result;
    $error |= false === fclose($pipes[1]);

// It is important that you close any pipes before calling
// proc_close in order to avoid a deadlock

    $return_value = proc_close($f);
    $error |= $return_value != 0;

    if (!$error && !$DO_DUMP) {
        // Disable cache.
        header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
        // We'll be outputting a PDF
        header('Content-type: application/pdf');
        header('Content-Transfer-Encoding: application/octet-stream');
        header('Content-Disposition: attachment; filename="' . $download_file_name . '"');
    } else {
        header("Content-Type: text/plain");
        if ($error) {
            echo "ERROR:\n";
            if ($return_value == -1)
                echo "Error accesing pdfformfiller.\n";
            else
                echo "pdfformfiller returned error code: $return_value\n\n";
        }
    }

    echo $result;
}

function fill_pdf_file_1($pipe, $fields) {
    $error = 0;
    foreach ( $fields as $key => $value) {
        $error |= false === fwrite($pipe, $key . ' ' . pdfff_escape( $value ) . "\n");
    }

    return $error;
}

function pdfff_escape($str) {
    //$str = str_replace("\\", "\\\\", $str);
    //$str = str_replace("\n", "\\n", $str);
    mb_internal_encoding('UTF-8');
    mb_regex_encoding('UTF-8');

    if (($s = mb_ereg_replace("\\", "\\\\", $str, "p")) !== false)
        $str = $s;

    // U+2028 utf-8 E280A8 : LINE SEPARATOR LS
    if (($s = mb_ereg_replace("\xE2\x80\xA8", "\\n", $str, "p")) !== false)
        $str = $s;

    //U+2029 utf-8 E280A9 : PARAGRAPH SEPARATOR PS
    if (($s = mb_ereg_replace("\xE2\x80\xA8", "\\p", $str, "p")) !== false)
        $str = $s;

    // DOS newline
    if (($s = mb_ereg_replace("\r\n", "\\n", $str, "p")) !== false)
        $str = $s;

    if (($s = mb_ereg_replace("\n", "\\n", $str, "p")) !== false)
        $str = $s;
    return $str;
}

function pdfff_checkbox($value) {
    if ($value)
        return 'Yes';
    return 'Off';
}


?>


