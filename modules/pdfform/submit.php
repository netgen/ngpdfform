<?php

$http = eZHTTPTool::instance();
$pdfFormINI = eZINI::instance('pdfform.ini');
$Module = $Params['Module'];

if ( !$_POST )
    return $Module->handleError( eZError::KERNEL_NOT_AVAILABLE, 'kernel' );

if ( !$pdfFormINI->hasVariable('Submit', 'QuestionsMap') ||
    !$pdfFormINI->hasVariable('Submit', 'SurveyContentObjectID') )
    return $Module->handleError( eZError::KERNEL_NOT_AVAILABLE, 'kernel' );


$questionsMap =  $pdfFormINI->variable('Submit', 'QuestionsMap');
$surveyContentObjectID =  $pdfFormINI->variable('Submit', 'SurveyContentObjectID');

$surveyObject = eZContentObject::fetch( (int) $surveyContentObjectID );
$dataMap = $surveyObject->attribute('data_map');
$eZContentObjectAttribute = $dataMap['survey'];

$ezSurvey = eZSurvey::fetchByObjectInfo( (int) $surveyContentObjectID, $eZContentObjectAttribute->attribute('contentclassattribute_id'), $eZContentObjectAttribute->attribute('language_code') );

if ( !$ezSurvey instanceof eZSurvey )
    return $Module->handleError( eZError::KERNEL_NOT_AVAILABLE, 'kernel' );

$surveyID = $ezSurvey->attribute('id');

$questionList = $ezSurvey->fetchQuestionList();

$params = array( 'prefix_attribute' => eZSurveyType::PREFIX_ATTRIBUTE,
                     'contentobjectattribute_id' => $eZContentObjectAttribute->attribute('id') );
$answers = array();
$db = eZDB::instance();
foreach( $questionsMap as $key => $val)
{
    // find the updated question id
    $result = $db->arrayQuery( "SELECT id FROM ezsurveyquestion WHERE original_id=(SELECT DISTINCT original_id FROM ezsurveyquestion WHERE id=$val) ORDER BY id DESC LIMIT 1" );
    $updatedQuestionID = $result[0]['id'];

    if ( strpos( $key, '_cb' ) !== false )
        $checkboxValue = true;

    $name = $params['prefix_attribute'] . '_ezsurvey_answer_' . $updatedQuestionID . '_' . $params['contentobjectattribute_id'];
    if ( $checkboxValue )
        $answers[$name][] = $http->variable( $key, '');
    else
        $answers[$name] = $http->variable( $key, '');

    $checkboxValue = false;
}

//print_r($answers);die();
$answers['SurveyStoreButton'] = 1;
if ($http->hasVariable( 'id'))
    $answers['ObjectID'] = $http->variable( 'id');
$answers['ContentObjectAttributeID'] = $eZContentObjectAttribute->attribute('id');
$answers['NodeID'] = $surveyObject->attribute('main_node_id');
$answers['SurveyID'] = $surveyID;

if ($http->hasPostVariable( 'user_id'))
    $answers['user_id'] = $http->postVariable( 'user_id');

if ( $http->hasPostVariable( 'pdfURI' ) && $http->postVariable( 'pdfURI' ) != '' )
    $answers['pdfURI'] = $http->postVariable( 'pdfURI' );

if ( $http->hasPostVariable( 'contentObjectUrlAlias' ) && $http->postVariable( 'contentObjectUrlAlias' ) != '' )
    $answers['contentObjectUrlAlias'] = $http->postVariable( 'contentObjectUrlAlias' );
//$answers['Hidden'] = 0;

$url = "survey/action";
$ignoreIndexDir = false;
if ( $pdfFormINI->hasVariable('Submit', 'IgnoreIndexDirInActionURL') && $pdfFormINI->variable('Submit', 'IgnoreIndexDirInActionURL') == 'enabled' )
{
    $ignoreIndexDir = true;

    if ( $pdfFormINI->hasVariable('Submit', 'SubmitActionUrlPrefix') )
    {
        $url =  $pdfFormINI->variable('Submit', 'SubmitActionUrlPrefix') . "/" . $url;
    }
}

eZURI::transformURI($url,$ignoreIndexDir,'full');

$ch = curl_init();
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded; charset=utf-8'));
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
if ($pdfFormINI->hasVariable('Submit', 'SurveyBasicAuth'))
{
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC );
    curl_setopt($ch, CURLOPT_USERPWD, $pdfFormINI->variable('Submit', 'SurveyBasicAuth'));
}
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($answers));
//curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

$result = curl_exec($ch);
if(curl_errno($ch))
{
    eZDebug::writeError( "CURL ERROR: " . curl_error($ch) . " " . $url,
                            'pdfform/submit.php' );
}else{
    eZDisplayResult( $result );
}
curl_close($ch);

eZExecution::cleanExit();

?>
