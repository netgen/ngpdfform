<?php

$Module = $Params['Module'];

if (!is_numeric($ObjectID))
    eZExecution::cleanExit();

$object = eZContentObject::fetch( $ObjectID );

if (!is_object($object))
    eZExecution::cleanExit();

$pdfFormINI = eZINI::instance('pdfform.ini');

$classIdentifier = $object->attribute('class_identifier');
$classIdentifierParts = explode( '_', $classIdentifier );
$type = strtoupper( $classIdentifierParts[1] );

if ( $type != 'AP' && $type != 'LD' )
    return $Module->handleError( eZError::KERNEL_NOT_AVAILABLE, 'kernel' );

$jsFiles = $pdfFormINI->variable( 'PopupSettings', 'JScriptFiles' );
$cssFiles = $pdfFormINI->variable( 'PopupSettings', 'CSSFiles' );

if ( count( $jsFiles ) )
{
    $jsFilesPath = array();
    foreach( $jsFiles as $_jsFile )
    {
        //eZURLOperator::eZDesign( eZTemplate::factory(), 'design:files/bla.pdf', 'ezdesign' );
        $jsFilesPath[] = eZURLOperator::eZDesign( eZTemplate::factory(), "javascript/$_jsFile", "ezdesign" );
    }
}

if ( count( $cssFiles ) )
{
    $cssFilesPath = array();
    foreach( $cssFiles as $_cssFile )
    {
        //eZURLOperator::eZDesign( eZTemplate::factory(), 'design:files/bla.pdf', 'ezdesign' );
        $cssFilesPath[] = eZURLOperator::eZDesign( eZTemplate::factory(), "stylesheets/$_cssFile", "ezdesign" );
    }
}

$user = eZUser::currentUser();

// creating FDF file
if ( $pdfFormINI->hasVariable('Download', 'AttributeMap') )
{
    $fields = array();
    $objectDataMap = $object->attribute('data_map');
    $preselectedMap = $pdfFormINI->variable('Download', 'PreSelectedMap');
    foreach( $pdfFormINI->variable('Download', 'AttributeMap') as $key => $multiValue)
    {
        if ( $multiValue == 'ObjectID' )
        {
            $fields[$key] = $ObjectID;
        }
        elseif( $multiValue == 'userID' )
        {
            $fields[$key] = $user->attribute('contentobject_id');
        }
        elseif( $multiValue == 'userObjectName' )
        {
            $fields[$key] = $user->attribute('contentobject')->attribute('name');
        }
        elseif( $multiValue == 'userObjectEmail' )
        {
            $fields[$key] = $user->attribute('email');
        }
        elseif( $multiValue == 'preselected' )
        {
            if ( !isset( $preselectedMap[$key] ) )
            {
                eZDebug::writeError( "Value $value in AttributeMap[] is used in combination with PreSelectedMap!. Please Map the $key with attribute identifier(eg. PreSelectedMap[$key]=attrib_name)" );
            }
            if (!isset( $objectDataMap[$preselectedMap[$key]] ) )
            {
                eZDebug::writeError( "Attribute \"$preselectedMap[$key]\" not exists in current object!" );
            }
            $attributeObject = $objectDataMap[$preselectedMap[$key]];
            $fields[$key] = false;
            if( $attributeObject->attribute('has_content') )
            {
                $fields[$key] = true;
            }
        }
        else
        {
            //explode values by comma from AttributeMap in ini file
            $valueArray = explode( ',', $multiValue );

            $multipleFields = array();
            foreach( $valueArray as $value )
            {
                $attributeObject = $objectDataMap[$value];

                if ( $value === 'agency' || !is_null( $attributeObject ) )
                {
                    if ( $value === 'agency' )
                    {
                        $ldFolderDataMap = $object->attribute('main_node')->attribute('parent')->attribute('data_map');
                        $ldFolderAgency = $ldFolderDataMap['agency'];
                        $agencyDataMap = $ldFolderAgency->attribute('content')->attribute('data_map');
                        $agencyShortName = $agencyDataMap['short_name'];
                        $multipleFields[] = $agencyShortName->attribute('content');
                    }
                    elseif ( $attributeObject->attribute('content') instanceof eZTags )
                    {
                        $multipleFields[] = $attributeObject->attribute('content')->attribute('keyword_string');
                    }
                    elseif ( $attributeObject->attribute('content') instanceof eZBirthday )
                    {
                        $tpl = eZTemplate::factory();
                        $tpl->setVariable( "attribute", $attributeObject );
                        $multipleFields[] = $tpl->fetch( "design:content/datatype/view/ezbirthday.tpl" );

                    }
                    else
                    {
                        $multipleFields[] = $attributeObject->attribute('content');
                    }

                }
                else
                {
                     eZDebug::writeError( "Value $value in AttributeMap[] not exists as attribute in current object!" );
                }
            }
            //assign values to key from ini file
            //$fields[$key] = 'TESTING 1234 čćžđš ČĆŽŠĐ';
            $delimiter = ', ';
            $delimiters = $pdfFormINI->variable( 'Download', 'AttributeMapDelimiter' );
            if ( $delimiters[$key] )
            {
                $delimiter = str_replace( '|', ' ', $delimiters[$key] );
            }

            $fields[$key] = implode("$delimiter", $multipleFields );
        }

    }
}

$tpl = eZTemplate::factory();

foreach( $fields as $filedVarKey => $filedVarValue )
{
    $tpl->setVariable( $filedVarKey, $filedVarValue );
}
///eng/pdfform/submit/review
$url = "pdfform/submit/review";
eZURI::transformURI($url,false,'full');

$tpl->setVariable( 'url', $url );

if ( $jsFilesPath )
    $tpl->setVariable( 'js_files_path', $jsFilesPath );

if ( $cssFilesPath )
    $tpl->setVariable( 'css_files_path', $cssFilesPath );

$result = $tpl->fetch( "design:pdfform/popup.tpl" );

echo $result;
eZExecution::cleanExit();
?>
