{ezscript_require(array('ezjsc::jquery', 'ezjsc::jqueryio'))}
<div class="warning" style="display: none;"></div>
<div class="success" style="display: none;"></div>
<script>
{literal}
jQuery(function( $ )
{
    $.ez( 'ngpdf::submit', {/literal}{$post_params}{literal}, function(data){
    	var output = '';
    	if( data.error_text == '' ){
    		output = '<h2>Form sent succesfully.</h2>';
    		output += '<p>Thank you for submiting form</p>';
    		$('.success').append(output).show();
    	}else{
    		output = '<h2>Form submit unsuccessful</h2>';
    		//output += '<p>'+data.error_text+'</p>';
    		output += '<p>Please contact the web adminstrator</p>';
    		$('.warning').append(output).show();
    	}
    });
});
{/literal}
</script>
